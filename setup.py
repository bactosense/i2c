#!/usr/bin/env python
# -*- coding: UTF8 -*-
'''
i2c
~~~

A Python extension for i2c communication.

:author: Douglas Watson <douglas.watson@epfl.ch>
:date: 2015
:license: GNU GPL v. 3.0

'''

from distutils.core import setup, Extension

setup(
    name="i2c",
    version="0.1",
    description="A simple python extension for i2c-dev functions.",
    author="Douglas C. Watson",
    author_email="douglas.watson@epfl.ch",
    license="GPLv3",
    ext_modules=[
        Extension("i2c", ["i2cmodule.c"])
    ]
)
