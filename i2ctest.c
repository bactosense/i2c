/*

i2ctest
-------

Testing reading the TI temperature sensor.

(c) 2015, Douglas C. Watson <douglas.watson@epfl.ch>

License: GNU GPL.

*/

#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <errno.h>
#include <unistd.h>

// static PyObject *I2CError;


/* ------------------------------------------
 * PURE C FUNCTIONS
 * ------------------------------------------ */

/*
 * _write writes an (array of) int starting from register `reg`.
 */
int
_write(int fd, uint8_t addr, uint8_t reg, uint8_t * buffer, unsigned int len)
{
    int retval;
    struct i2c_msg wmsg = {
        .addr = addr & 0xFF,
        .flags = 0,
        .len = 1 + len, // Keep space for register address
    };

    struct i2c_rdwr_ioctl_data transfer_descriptor = {
        .msgs = &wmsg,
        .nmsgs = 1,
    };

    // Allocate the message buffer. The first byte is address of device
    wmsg.buf = malloc(wmsg.len);
    if (wmsg.buf == NULL)
        return -ENOMEM;

    wmsg.buf[0] = reg & 0xFF;
    memcpy(wmsg.buf + 1, buffer, len);

    if ( !fd ) {
        free(wmsg.buf);
        return (-1);    /* device has not been opened */
    }
    // Configure dev file and write
    if ( (retval = ioctl(fd, I2C_SLAVE, addr)) < 0 ) {
        free(wmsg.buf);
        return (retval);
    }
    // This line actually does the writing
    if ( (retval = ioctl(fd, I2C_RDWR, &transfer_descriptor)) < 0 ) {
        free(wmsg.buf);
        return (retval);
    }

    free(wmsg.buf);
    return (0);
}

/*
 * _read reads `len` ints into `buffer` starting from register `reg`.
 */
int
_read(int fd, uint8_t addr, uint8_t reg, uint8_t * buffer, unsigned int len)
{
    int retval;     // Return value

    struct i2c_msg wmsgs[2] = {
        {
            .addr = addr,
            .flags = 0,
            .len = 1,
            .buf = &reg
        },
        {
            .addr = addr,
            .flags = I2C_M_RD,
            .len = len,
            .buf = buffer
        }
    };

    struct i2c_rdwr_ioctl_data transfer_descriptor = {
        .msgs = wmsgs,
        .nmsgs = 2
    };

    if ( !fd ) {
        return -1;
    }
    if ( (retval = ioctl(fd, I2C_SLAVE, addr)) < 0 )
        return (retval);

    return (ioctl(fd, I2C_RDWR, &transfer_descriptor));
}

/*
 * _read_only reads `len` bytes into `buffer`, *not* prceded by an address
 * write. This is purely a read operation.
 */
int
_read_only(int fd, uint8_t addr, uint8_t *buffer, unsigned int len) {

    int retval;

    struct i2c_msg wmsg = {
        .addr = addr & 0xFF,
        .flags = I2C_M_RD,
        .len = len,
        .buf = buffer
    };

    struct i2c_rdwr_ioctl_data transfer_descriptor = {
        .msgs = &wmsg,
        .nmsgs = 1
    };

    if ( !fd ) {
        return -1;
    }
    if ( (retval = ioctl(fd, I2C_SLAVE, addr)) < 0 )
        return (retval);

    return (ioctl(fd, I2C_RDWR, &transfer_descriptor));

}

/* write_read writes and reads in one shot. It writes wlen bytes from wbuf, then
reads rlen bytes into rbuf.

For a pure read operation, make wbuf a single byte containing the register
address  to read from. To write, set rlen to 0 and the first byte of wlen to the
address.

 */
int
_write_read(int fd, uint8_t addr, uint8_t *wbuf, uint8_t *rbuf,
            unsigned int wlen, unsigned int rlen)
{

    int retval;     // Return value

    struct i2c_msg wmsgs[2] = {
        {
            .addr = addr,
            .flags = 0,
            .len = wlen,
            .buf = wbuf
        },
        {
            .addr = addr,
            .flags = I2C_M_RD,
            .len = rlen,
            .buf = rbuf
        }
    };

    struct i2c_rdwr_ioctl_data transfer_descriptor =
    {
        .msgs    = wmsgs,
        .nmsgs   = 2
    };

    if ( !fd ) {
        return -1;
    }

    if ( (retval = ioctl(fd, I2C_SLAVE, addr)) < 0 )
        return (retval);

    return (ioctl(fd, I2C_RDWR, &transfer_descriptor));

}


int main(void) {

    uint8_t addr = 0x40;
    uint8_t reg = 0x00;
    int len = 4;  // read two bytes for temperature
    int retval;  // capture return value of ioctl
    int fd;  // i2c dev file

    uint8_t buffer[len];

    // TODO: add a second read message for humidity.
    struct i2c_msg wmsg2 = {
        .addr = addr & 0xFF,
        .flags = I2C_M_RD,
        .len = len,
        .buf = buffer
    };

    struct i2c_rdwr_ioctl_data transfer_descriptor2 = {
        .msgs = &wmsg2,
        .nmsgs = 1
    };


    // Write to register to request temperature read

    fd = open("/dev/i2c-0", O_RDWR);

    if ( !fd ) {
        return -1;
    }
    if ( (retval = ioctl(fd, I2C_SLAVE, addr)) < 0 )
        return (retval);

    // retval = ioctl(fd, I2C_RDWR, &transfer_descriptor);
    retval = _write(fd, addr, 0x00, NULL, 0);
    if ( retval < 0 ) {
        printf("Write failed");
    }

    sleep(1);

    // Read response.
    // retval = ioctl(fd, I2C_RDWR, &transfer_descriptor2);
    retval = _read_only(fd, addr, buffer, 4);

    printf("%d %d %d %d", buffer[0], buffer[1], buffer[2], buffer[3]);
}
