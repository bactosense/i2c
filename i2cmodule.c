/*

i2c
---

An i2c extension for Python.

(c) 2015, Douglas C. Watson <douglas.watson@epfl.ch>
(c) 2021, Adrian Shajkofci

License: GNU GPL.

*/

#include <Python.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <errno.h>

// static PyObject *I2CError;


/* ------------------------------------------
 * PURE C FUNCTIONS
 * ------------------------------------------ */

/*
 * _write writes an (array of) int starting from register `reg`.
 */
int
_write(int fd, uint8_t addr, uint8_t reg, uint8_t * buffer, unsigned int len)
{
    int retval;
    struct i2c_msg wmsg = {
        .addr = addr & 0xFF,
        .flags = 0,
        .len = 1 + len, // Keep space for register address
    };

    struct i2c_rdwr_ioctl_data transfer_descriptor = {
        .msgs = &wmsg,
        .nmsgs = 1,
    };

    // Allocate the message buffer. The first byte is address of device
    wmsg.buf = malloc(wmsg.len);
    if (wmsg.buf == NULL)
        return -ENOMEM;

    wmsg.buf[0] = reg & 0xFF;
    memcpy(wmsg.buf + 1, buffer, len);

    if ( !fd ) {
        free(wmsg.buf);
        return (-1);    /* device has not been opened */
    }
    // Configure dev file and write
    if ( (retval = ioctl(fd, I2C_SLAVE, addr)) < 0 ) {
        free(wmsg.buf);
        return (retval);
    }
    // This line actually does the writing
    if ( (retval = ioctl(fd, I2C_RDWR, &transfer_descriptor)) < 0 ) {
        free(wmsg.buf);
        return (retval);
    }

    free(wmsg.buf);
    return (0);
}

/*
 * _read reads `len` ints into `buffer` starting from register `reg`.
 */
int
_read(int fd, uint8_t addr, uint8_t reg, uint8_t * buffer, unsigned int len)
{
    int retval;     // Return value

    struct i2c_msg wmsgs[2] = {
        {
         .addr = addr,
         .flags = 0,
         .len = 1,
         .buf = &reg},
        {
         .addr = addr,
         .flags = I2C_M_RD,
         .len = len,
         .buf = buffer}
    };

    struct i2c_rdwr_ioctl_data transfer_descriptor = {
        .msgs = wmsgs,
        .nmsgs = 2
    };

    if ( !fd ) {
        return -1;
    }
    if ( (retval = ioctl(fd, I2C_SLAVE, addr)) < 0 )
        return (retval);

    return (ioctl(fd, I2C_RDWR, &transfer_descriptor));
}

/*
 * _read_only reads `len` bytes into `buffer`, *not* prceded by an address
 * write. This is purely a read operation.
 */
int
_read_only(int fd, uint8_t addr, uint8_t *buffer, unsigned int len) {

    int retval;

    struct i2c_msg wmsg = {
        .addr = addr & 0xFF,
        .flags = I2C_M_RD,
        .len = len,
        .buf = buffer
    };

    struct i2c_rdwr_ioctl_data transfer_descriptor = {
        .msgs = &wmsg,
        .nmsgs = 1
    };

    if ( !fd ) {
        return -1;
    }
    if ( (retval = ioctl(fd, I2C_SLAVE, addr)) < 0 )
        return (retval);

    return (ioctl(fd, I2C_RDWR, &transfer_descriptor));

}

/* write_read writes and reads in one shot. It writes wlen bytes from wbuf, then
reads rlen bytes into rbuf.

For a pure read operation, make wbuf a single byte containing the register
address  to read from. To write, set rlen to 0 and the first byte of wlen to the
address.

 */
int
_write_read(int fd, uint8_t addr, uint8_t *wbuf, uint8_t *rbuf,
            unsigned int wlen, unsigned int rlen)
{

    int retval;     // Return value

    struct i2c_msg wmsgs[2] = {
        {
         .addr = addr,
         .flags = 0,
         .len = wlen,
         .buf = wbuf},
        {
         .addr = addr,
         .flags = I2C_M_RD,
         .len = rlen,
         .buf = rbuf}
    };

    struct i2c_rdwr_ioctl_data transfer_descriptor =
    {
        .msgs    = wmsgs,
        .nmsgs   = 2
    };

    if ( !fd ) {
        return -1;
    }

    if ( (retval = ioctl(fd, I2C_SLAVE, addr)) < 0 )
        return (retval);

    return (ioctl(fd, I2C_RDWR, &transfer_descriptor));

}

/* ------------------------------------------
 * PYTHON WRAPPERS (AND HELPERS)
 * ------------------------------------------ */

/* Parse Python list object, return pointer to newly allocated
   array and set len to length of the new array.

    */
uint8_t * parse_list(PyObject * listobj, unsigned int *len)
{
    PyObject *seq;      // Sequence extracted from that list
    PyObject *item;     // Item of the sequence
    uint8_t *wbuf;         // C array to transfer the values to

    seq = PySequence_Fast(listobj, "`wbytes` must be a list");
    (*len) = PySequence_Fast_GET_SIZE(seq);

    // Allocate C array and populate it
    wbuf = malloc(*len);
    if ( wbuf == NULL ) {
        PyErr_NoMemory();
        return NULL;
    }

    long int i, tmp;
    // Shift indexing if we need to insert address at beginning
    for ( i = 0 ; i < *len ; i++ ) {
        item = PySequence_Fast_GET_ITEM(seq, i);
        tmp = PyLong_AsUnsignedLongMask(item);
        if ( tmp < 0) {
            return NULL;
        }

        wbuf[i] = (uint8_t) tmp;
    }

    return wbuf;
}

PyObject *build_list(uint8_t * carray, unsigned int len)
{
    PyObject * pylist = PyList_New(len);

    int i;
    for ( i = 0 ; i < len ; i++ ) {
        PyList_SET_ITEM(pylist, i, Py_BuildValue("i", (int)carray[i]));
    }

    return pylist;
}

static PyObject *i2c_get(PyObject * self, PyObject * args)
{
    int rc;             // Function return code
    char fpath[16];     // Path to /dev/i2c-* file
    uint8_t *buffer;    // Write results to this array

    // Python function parameters:
    uint8_t bus;    // I2C bus number
    uint8_t addr;       // 8-bit device address
    uint8_t reg;        // Register to start reading from
    int n;    // How many bytes to read

    // Parse arguments
    if ( !PyArg_ParseTuple(args, "iiii", &bus, &addr, &reg, &n) )
        return NULL;

    // Open i2c bus
    sprintf(fpath, "/dev/i2c-%d", bus);
    int fd = open(fpath, O_RDWR);
    if ( fd < 0 ) {
        PyErr_SetFromErrno(PyExc_IOError);
        return NULL;
    }

    // Prepare reception:
    buffer = malloc(n);
    if ( buffer == NULL ) {
        PyErr_NoMemory();
        close(fd);
        return NULL;
    }

    // Read
    uint8_t wbuf[1] = {reg};

    rc = _write_read(fd, addr, wbuf, buffer, 1, n);
    if (rc < 0) {
        PyErr_SetFromErrno(PyExc_IOError);
        close(fd);
        free(buffer);
        return NULL;
    }

    close(fd);

    return build_list(buffer, n);
}

static PyObject *i2c_get_simple(PyObject * self, PyObject * args)
{
    int rc;             // Function return code
    char fpath[16];     // Path to /dev/i2c-* file
    uint8_t *buffer;    // Write results to this array

    // Python function parameters:
    uint8_t bus;    // I2C bus number
    uint8_t addr;       // 8-bit device address
    int n;    // How many bytes to read

    // Parse arguments
    if ( !PyArg_ParseTuple(args, "iii", &bus, &addr, &n) )
        return NULL;

    // Open i2c bus
    sprintf(fpath, "/dev/i2c-%d", bus);
    int fd = open(fpath, O_RDWR);
    if ( fd < 0 ) {
        PyErr_SetFromErrno(PyExc_IOError);
        return NULL;
    }

    // Prepare reception:
    buffer = malloc(n);
    if ( buffer == NULL ) {
        PyErr_NoMemory();
        close(fd);
        return NULL;
    }

    // Read
    rc = _read_only(fd, addr, buffer, n);
    if (rc < 0) {
        PyErr_SetFromErrno(PyExc_IOError);
        close(fd);
        free(buffer);
        return NULL;
    }

    close(fd);

    return build_list(buffer, n);
}

static PyObject *i2c_set(PyObject * self, PyObject * args)
{
    int rc;             // Function return code
    char fpath[16];     // Path to /dev/i2c-* file

    // Python function parameters:
    uint8_t bus;            // I2C bus number
    uint8_t addr;           // 8-bit device address
    uint8_t reg;            // Register to start reading from
    // uint8_t rbuf[0];          // Read buffer (will stay empty)
    uint8_t *wbuf;          // Values to write
    unsigned int wlen;      // How many values to write
    PyObject *listobj;      // List of write bytes, before conversion to C.


    // Parse arguments
    if ( !PyArg_ParseTuple(args, "iiiO", &bus, &addr, &reg, &listobj) )
        return NULL;
    wbuf = parse_list(listobj, &wlen);  // This sets wlen
    if ( wbuf == NULL ) {
        PyErr_SetString(PyExc_TypeError,
            "`bytes` argument must be a list of int."
        );
        return NULL;
    }

    // Open i2c bus
    sprintf(fpath, "/dev/i2c-%d", bus);
    int fd = open(fpath, O_RDWR);
    if ( fd < 0 ) {
        PyErr_SetFromErrno(PyExc_IOError);
        free(wbuf);
        return NULL;
    }

    // Write values (and read none)
    rc = _write(fd, addr, reg, wbuf, wlen);
    if (rc < 0) {
        PyErr_SetFromErrno(PyExc_IOError);
        close(fd);
        free(wbuf);
        return NULL;
    }

    close(fd);
    free(wbuf);

    Py_RETURN_NONE;
}

static PyMethodDef I2CMethods[] = {
    {"get", i2c_get, METH_VARARGS,
        "get(bus, addr, reg, n) -> list()\n\n"
        "Read n registers from `reg`, return list of int.",
    },
    {"set", i2c_set, METH_VARARGS,
        "set(bus, addr, reg, bytes) -> None\n\n"
        "Write `bytes` (array of int) to i2c device.\n\n"
        "**WARNING**: Segfaults if `bytes` is not a list!",
    },
    {"get_simple", i2c_get_simple, METH_VARARGS,
        "get(bus, addr, n) -> list()\n\n"
        "Read n registers from device, return list of int.\n\n"
        "This function is a pure read, it does not issue a write request first,\n"
        "to specify a register for example."
    },
};

PyMODINIT_FUNC
#if PY_MAJOR_VERSION >= 3
PyInit_i2c(void)
#else
initi2c(void)
#endif
{
    #if PY_MAJOR_VERSION >= 3
        static struct PyModuleDef moduledef = {
            PyModuleDef_HEAD_INIT,
            "i2c",     /* m_name */
            "i2cmodule",  /* m_doc */
            -1,                  /* m_size */
            I2CMethods,    /* m_methods */
            NULL,                /* m_reload */
            NULL,                /* m_traverse */
            NULL,                /* m_clear */
            NULL,                /* m_free */
        };
    #endif

    #if PY_MAJOR_VERSION >= 3
        PyObject *m = PyModule_Create(&moduledef);
    #else
        PyObject *m = Py_InitModule3("i2c", I2CMethods);

        if (m == NULL)
            return;
        return m;
    #endif

    if (m == NULL)
        return;
}
