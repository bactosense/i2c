i2c
===

`i2c` is a Python extension to access i2c devices, through the user-space C `i2c-dev` library. 

Currently, `i2c` provides two functions, `get` and `set`:

    bus = 0  # i2c bus 0
    addr = 0x63  # Device address on bus 0
    status_reg = 0x00  # status register
    command_reg = 0x40  # Write commands to this register
    
    # Read four bytes from the status register
    status = i2c.get(bus, addr, status_reg, 4)
    # --> status = [7, 0, 0, 0]
    
    # Write a single byte the command register
    i2c.set(bus, addr, command_reg, [0x21])

`get` can retrieve any number of bytes. `set` can write any number of bytes.


Warnings
--------

1. `set` segfaults if you give it anything else than a list as last argument! The C python API provides an argument parsing function, which unfortunatly segfaults if it doesn't receive the expected parameter. I haven't found a workaround.

2. Make sure your i2c requests are not issued too fast; timing is your responsibility. For example, if you instruct the power supply to turn on the laser, then immediately after ask it if the laser is on, the second query will probably be treated before the laser is turned on, and therefore would return negative.


Installation
------------

Install as usual for a Python package:

    python setup.py install

Note that you must have `i2c-dev.h` available.

On ARM, you will probably have to cross-compile it. The easiest way I found is to add a package to Buildroot. Just copy the folder of another python package in `packages/` and modify the obvious variables in the makefile.

Limitations
-----------

This module provides no functionality for:

- 10 bit addressing
- Writing and reading in the same operation

In addition, it provides minimal error handling: it returns standard error codes, but no easily caught "I2CError" (yet). I would recommend implementing that at the application level, in whatever layer manages your i2c interfaces.
